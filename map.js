function map(obj, cb) {
    const arr = [];
    for (let val of Object.keys(obj)) {
        arr.push(cb(obj[val]));
    }
    return arr;
}

module.exports = map;