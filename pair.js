function pair(obj) {
    const list = Object.keys(obj).map((key) => [(key), obj[key]]);

    return list;
}

module.exports = pair;