function invert(obj) {
    const newObj = Object.keys(obj).reduceRight(function (accum, key, i) {
        accum[obj[key]] = key;
        return accum;
    }, {})
    return newObj;
}

module.exports = invert;