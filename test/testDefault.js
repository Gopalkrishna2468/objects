const defaults = require("../defaults");

const obj = {
    name: 'Gopal',
    age: 10,
    dress: 'shirt'
};

const defaultProps = {
    name: 'ramu',
    age: 36,
    dress: 'phant',
    bestFriend: 'hari',
    enemy: 'reins'
};

console.log(defaults(obj, defaultProps));